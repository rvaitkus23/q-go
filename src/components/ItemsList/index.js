import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ItemsListRow from '../ItemsListRow';
import {
  removeItem,
  toggleItem,
  toggleCompletedVisibility,
} from '../../logic/todos';
import './styles.css';

export const ItemsList = ({
  items,
  hideDoneItems,
  onRemove,
  onToggleDone,
  toggleCompletedVisibility,
}) => {
  return (
    <div className="ItemsList-container">
      {items.length < 1 ? (
        <p id="items-missing">Add some tasks above.</p>
      ) : (
        items.length > 0 && (
          <div>
            <button
              className={
                hideDoneItems && 'ItemList-completedVisibilityToggle--active'
              }
              onClick={toggleCompletedVisibility}
            >
              Hide completed items
            </button>
          </div>
        )
      )}
      {items.map(item => (
        <ItemsListRow
          key={item.id}
          item={item}
          onToggleDone={() => onToggleDone(item.id)}
          onRemove={() => onRemove(item.id)}
        />
      ))}
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  hideDoneItems: PropTypes.bool.isRequired,
  onRemove: PropTypes.func.isRequired,
  onToggleDone: PropTypes.func.isRequired,
  toggleCompletedVisibility: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  items: state.todos.items.filter(
    item => !state.todos.hideDoneItems || !item.done,
  ),
  hideDoneItems: state.todos.hideDoneItems,
});

const mapDispatchToProps = dispatch => ({
  onRemove: id => dispatch(removeItem(id)),
  onToggleDone: id => dispatch(toggleItem(id)),
  toggleCompletedVisibility: () => dispatch(toggleCompletedVisibility()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
