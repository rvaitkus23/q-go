import React from 'react';
import { shallow } from 'enzyme';
import { ItemsList } from '../index';
import { ItemsListRow } from '../../ItemsListRow/index';

const defaultProps = {
  items: [],
  hideDoneItems: false,
};

describe('ItemsList', () => {
  it('renders without crashing', () => {
    shallow(<ItemsList {...defaultProps} />);
  });

  it('should display warning message if no items', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} items={[]} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(1);
  });

  it('should not display warning message if items are present', () => {
    const items = [{ id: 1, content: 'Test 1', done: false }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(0);
  });

  it('should render items as list items', () => {
    const items = [
      { id: 1, content: 'Test 1', done: false },
      { id: 2, content: 'Test 2', done: false },
    ];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find(ItemsListRow)).toHaveLength(2);
  });

  it('should render filtered items if Hide Done Items is active', () => {
    const items = [
      { id: 1, content: 'Test 1', done: false },
      { id: 2, content: 'Test 2', done: true },
    ];
    const renderedItem = shallow(
      <ItemsList {...defaultProps} hideDoneItems={true} items={items} />,
    );
    expect(renderedItem.find(ItemsListRow)).toHaveLength(1);
  });

  it('should trigger onRemove item on button clicked', () => {
    const onRemoveMock = jest.fn();
    const items = [
      { id: 1, content: 'Test 1', done: false },
      { id: 2, content: 'Test 2', done: false },
    ];
    const renderedList = shallow(
      <ItemsList {...defaultProps} onRemove={onRemoveMock} items={items} />,
    );
    const renderedItems = renderedList.find(ItemsListRow);
    expect(renderedItems).toHaveLength(2);
    const removeButton = renderedItems
      .first()
      .dive()
      .find('button');
    expect(removeButton).toHaveLength(1);
    removeButton.simulate('click');
    expect(onRemoveMock.mock.calls.length).toBe(1);
    expect(onRemoveMock.mock.calls[0][0]).toBe(1);
  });

  it('should trigger onToggleDone item on checkbox clicked', () => {
    const onToggleDoneMock = jest.fn();
    const items = [
      { id: 1, content: 'Test 1', done: false },
      { id: 2, content: 'Test 2', done: false },
    ];
    const renderedList = shallow(
      <ItemsList
        {...defaultProps}
        onToggleDone={onToggleDoneMock}
        items={items}
      />,
    );
    const renderedItems = renderedList.find(ItemsListRow);
    expect(renderedItems).toHaveLength(2);
    const checkbox = renderedItems
      .last()
      .dive()
      .find('input[type="checkbox"]');
    expect(checkbox).toHaveLength(1);
    checkbox.simulate('change', { target: { checked: true } });
    expect(onToggleDoneMock.mock.calls.length).toBe(1);
    expect(onToggleDoneMock.mock.calls[0][0]).toBe(2);
  });
});
