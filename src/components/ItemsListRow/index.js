import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export const ItemsListRow = ({ item, onToggleDone, onRemove }) => {
  return (
    <div className="ItemsListRow">
      <div>
        <input type="checkbox" checked={item.done} onChange={onToggleDone} />
      </div>
      <div
        className={`ItemsListRow-text ${
          item.done ? 'ItemsListRow-text--done' : ''
        }`}
      >
        {item.content}
      </div>
      <div>
        <button onClick={onRemove}>Remove</button>
      </div>
    </div>
  );
};

ItemsListRow.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired,
  }).isRequired,
  onToggleDone: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default ItemsListRow;
