import reducer, {
  initialState,
  addItem,
  removeItem,
  toggleItem,
  toggleCompletedVisibility,
} from '../todos';

describe('reducer', () => {
  let state;

  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  beforeEach(() => {
    state = {
      items: [
        { id: 1, content: 'first', done: false },
        { id: 2, content: 'second', done: false },
      ],
      hideDoneItems: false,
    };
  });

  it('should add new items on ADD_ITEM', () => {
    const mockAction = addItem('third');
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });

  it('should remove item on REMOVE_ITEM', () => {
    const mockAction = removeItem(1);
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
    expect(result.items[0].id).toEqual(2);
    expect(result.items[0].content).toEqual('second');
  });

  it('should mark item as done on TOGGLE_ITEM', () => {
    const mockAction = toggleItem(1);
    const result = reducer(state, mockAction);
    expect(result.items[0].id).toEqual(1);
    expect(result.items[0].done).toEqual(true);
  });

  it('should change filterDoneItems on TOGGLE_COMPLETED_VISIBILITY', () => {
    const mockAction = toggleCompletedVisibility();
    const result = reducer(state, mockAction);
    expect(result.hideDoneItems).toEqual(true);
  });
});
