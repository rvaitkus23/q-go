export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const REMOVE_ITEM = 'qgo/assessment/REMOVE_ITEM';
export const TOGGLE_ITEM = 'qgo/assessment/TOGGLE_ITEM';
export const TOGGLE_COMPLETED_VISIBILITY =
  'qgo/assessment/TOGGLE_COMPLETED_VISIBILITY';

export const addItem = content => ({ type: ADD_ITEM, content });

export const removeItem = id => ({ type: REMOVE_ITEM, id });

export const toggleItem = id => ({ type: TOGGLE_ITEM, id });

export const toggleCompletedVisibility = () => ({
  type: TOGGLE_COMPLETED_VISIBILITY,
});

export const initialState = {
  items: [
    { id: 1, content: 'Call mum', done: false },
    { id: 2, content: 'Buy cat food', done: true },
    { id: 3, content: 'Water the plants', done: false },
  ],
  hideDoneItems: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
        done: false,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };
    case REMOVE_ITEM:
      const modifiedItems = [...state.items];
      const itemIndex = modifiedItems.findIndex(item => item.id === action.id);
      if (itemIndex >= 0) {
        modifiedItems.splice(itemIndex, 1);
      }
      return {
        ...state,
        items: modifiedItems,
      };
    case TOGGLE_ITEM:
      const updatedItems = [...state.items];
      const updatedItemIndex = updatedItems.findIndex(
        item => item.id === action.id,
      );
      if (updatedItemIndex >= 0) {
        updatedItems[updatedItemIndex].done = !updatedItems[updatedItemIndex]
          .done;
      }
      return {
        ...state,
        items: updatedItems,
      };
    case TOGGLE_COMPLETED_VISIBILITY:
      return {
        ...state,
        hideDoneItems: !state.hideDoneItems,
      };
    default:
      return state;
  }
};

export default reducer;
